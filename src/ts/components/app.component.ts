import { Component } from '@angular/core';

@Component ({
    
    selector: 'my-app',
    templateUrl: `app/templates/components/app.component.html`,
    styleUrls: [`app/css/components/app.component.css`]
    
})

export class AppComponent {
    
    title = 'Tour of Heroes';
    
}