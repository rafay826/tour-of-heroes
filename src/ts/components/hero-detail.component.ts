import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Hero } from '../models/hero';
import { HeroService } from '../services/hero.service';

@Component({
  selector: 'my-hero-detail',
  templateUrl: `app/templates/components/hero-detail.component.html`
})

export class HeroDetailComponent implements OnInit {
    
    ngOnInit(): void {
      this.route.params.forEach((params: Params) => {
        let id = +params['id'];
        this.heroService.getHero(id)
          .then(hero => this.hero = hero);
      });
    }
    
    constructor(
        private heroService: HeroService,
        private route: ActivatedRoute
    ){
    }
    
    @Input()
    hero: Hero;
     
    goBack(): void {
      window.history.back();
    }

    save(): void {
      this.heroService.update(this.hero)
        .then(this.goBack);
    }
    
}

